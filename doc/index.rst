.. PyGASP documentation master file, created by
   sphinx-quickstart2 on Tue Mar  1 23:08:37 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyGASP's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   toc_api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

